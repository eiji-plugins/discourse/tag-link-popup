class TagLinkPopupModeSetting
  def self.translate_names?
    false
  end

  def self.valid_value?(value)
    valid_values.any? { |mode| mode == value }
  end

  def self.values
    @values = valid_values.map { |mode|
      {
        name: I18n.t("site_settings.tag_link_popup_modes.#{mode}"),
        value: mode
      }
    }
  end

  def self.valid_values
    ["external_link", "internal_link", "popup"]
  end

  private_class_method :valid_values
end