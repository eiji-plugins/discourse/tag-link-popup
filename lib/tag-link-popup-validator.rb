class TagLinkPopupValidator
  def error_message
    case @failed
    when "slug"
      I18n.t("tag_link_popup.errors.validator.external_slug_does_not_exists", slug: @slug)
    when "tag"
      I18n.t("tag_link_popup.errors.validator.internal_tag_does_not_exists", tag: @tag)
    end
  end

  def initialize(opts = {})
    @opts = opts
  end

  def slug_exists?(slug)
    require "net/http"
    pattern = SiteSetting.tag_link_popup_external_tag_url_pattern
    url = URI.parse(pattern.sub("{tag-slug}", slug))
    request = Net::HTTP.new(url.host, url.port)
    request.use_ssl = (url.scheme == 'https')
    response = request.request_head(url.to_s)
    check = response.code == "200"
    if check
      check
    else
      @failed = "slug"
      @slug = slug
    end
  end

  def tag_exists?(tag)
    check = Tag.where(name: tag).exists?
    unless check
      @failed = "tag"
      @tag = tag
    end
    check
  end

  def valid_value?(value)
    value.split("|").all? { |string|
      if string.include? ":"
        array = string.split ":"
        tag_exists?(array[0]) && slug_exists?(array[1])
      else
        tag_exists?(string) && slug_exists?(string)
      end
    }
  end
end