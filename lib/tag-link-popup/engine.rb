module TagLinkPopup
  class Engine < ::Rails::Engine
    engine_name "TagLinkPopup".freeze
    isolate_namespace TagLinkPopup

    config.after_initialize do
      Discourse::Application.routes.append do
        mount ::TagLinkPopup::Engine, at: "/tag-link-popup"
      end
    end
  end
end
