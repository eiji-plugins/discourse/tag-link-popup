module TagLinkPopup
  class ImportParser
    def error
      if @error
        first_line = message("parse_error", @error)
        parts = []
        parts.push message("found_at", @error) if @error[:line]
        if @error[:index]
          error_with_count = {count: @error[:index]}.merge(@error)
          parts.push message("found_json_array_index", error_with_count)
        end
        parts.push message("found_json_key", @error) if @error[:key]
        parts.push message("found_json_value", @error) if @error[:value]
        first_line + "\n" + parts.map{|part| "  #{part}"}.join("\n")
      end
    end

    def parse(content, content_type)
      case content_type
      when "application/json"
        parse_json(content)
      when "text/csv"
        parse_csv(content)
      when "text/plain"
        parse_text(content)
      else
        @error = {message: "unsupported content type"}
        return
      end
    end

    private

    def parse_csv(content)
      content.split("\n").map.with_index(1) { |line, index|
        parts = line.split(",")
        case
        when parts.length != 2
          @error = {line: line, line_index: index, message: parser_message("csv", "expected_columns")}
          return
        when parts.any? { |part| invalid_string?(part) }
          @error = {line: line, line_index: index, message: parser_message("csv", "expected_row")}
          return
        when parts[0] == parts[1]
          [parts[0]]
        else
          parts
        end
      }
    end

    def parse_json(content)
      data = JSON.parse(content)
      case
      when data.instance_of?(Array)
        data.map.with_index(1) { |item, index|
          parsed = parse_json_array(item, index)
          return if @result
          parsed
        }
      when data.instance_of?(Hash)
        data.map { |key, value|
          parsed = parse_json_map(key, value)
          return if @result
          parsed
        }
      end
    end

    def parse_json_array(item, index)
      if invalid_string?(item)
        @error = {message: parser_message("json", "invalid_item"), index: index}
        return
      else
        [item]
      end
    end

    def parse_json_map(key, value)
      case
      when invalid_string?(key)
        @error = {message: parser_message("json", "invalid_key"), value: value}
        return
      when invalid_string?(value)
        @error = {message: parser_message("json", "invalid_value"), key: key}
        return
      when key == value
        [key]
      else
        [key, value]
      end
    end

    def parse_text(content)
      content.split("\n").map.with_index(1) { |line, index|
        parts = line.split(":")
        length = parts.length
        case
        when (length == 1 && invalid_string?(parts[0]))
          @error = {line: line, line_index: index, message: parser_message("text", "expected_tag")}
          return
        when (length == 2 && invalid_string?(parts[0]))
          @error = {line: line, line_index: index, message: parser_message("text", "expected_tag")}
          return
        when (length == 2 && invalid_string?(parts[1]))
          @error = {line: line, line_index: index, message: parser_message("text", "expected_slug")}
          return
        when [1, 2].include?(length) && parts[0] == parts[1]
          [parts[0]]
        when [1, 2].include?(length)
          parts
        else
          @error = {line: line, line_index: index, message: parser_message("text", "expected_tag_or_tag_slug")}
          return
        end
      }
    end

    def invalid_string?(string)
      case
      when (not string.instance_of?(String))
        return true
      when string.empty?
        return true
      else
        return false
      end
    end

    def message(code, data)
      I18n.t "tag_link_popup.errors.parser.#{code}", data
    end

    def parser_message(parser, code)
      I18n.t "tag_link_popup.errors.parsers.#{parser}.#{code}"
    end
  end
end
