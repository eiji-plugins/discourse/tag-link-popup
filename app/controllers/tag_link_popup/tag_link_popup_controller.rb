module TagLinkPopup
  class TagLinkPopupController < ::ApplicationController
    requires_plugin TagLinkPopup

    before_action :ensure_logged_in

    def import
      file = request.request_parameters["file"]
      content = parse(file.read(), file.content_type)
      if @error
        render json: @error
        return
      end
      validate(content)
      if @error
        render json: @error
        return
      end
      merge(content)
      render json: {parse: true, status: "ok", validate: true}
    end

    def index
    end

    private

    def merge(content)
      old_array = SiteSetting.tag_link_popup_tags_list.split("|")
      new_array = content.map{|parts| parts.join(":")}
      uniq_array = old_array | new_array
      SiteSetting.tag_link_popup_tags_list = uniq_array.join("|")
    end

    def parse(content, content_type)
      parser = TagLinkPopup::ImportParser.new
      data = parser.parse(content, content_type)
      error = parser.error
      @error = {message: error, parse: false, result: "error", validate: false} if error
      data
    end

    def validate(content)
      validator = TagLinkPopupValidator.new
      content.find { |parts|
        if parts.length == 1
          not(validator.tag_exists?(parts[0]) && validator.slug_exists?(parts[0]))
        else
          not(validator.tag_exists?(parts[0]) && validator.slug_exists?(parts[1]))
        end
      }
      message = validator.error_message
      if message
        @error = {message: message, parse: true, result: "error", validate: false}
      end
    end
  end
end
