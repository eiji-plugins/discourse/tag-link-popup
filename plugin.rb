# frozen_string_literal: true

# name: TagLinkPopup
# about: Change a standard tag link to popup with standard link and portal links.
# version: 1.0
# authors: Tomasz Marek Sulima
# url: https://github.com/Eiji7

register_asset 'stylesheets/tag-link-popup.scss'
register_editable_user_custom_field :tag_link_popup_mode
register_svg_icon "fa-external-link-alt" if respond_to?(:register_svg_icon)
register_svg_icon "fa-file-upload" if respond_to?(:register_svg_icon)
register_svg_icon "fab-discourse" if respond_to?(:register_svg_icon)
PLUGIN_NAME ||= 'TagLinkPopup'

load File.expand_path('lib/tag-link-popup/engine.rb', __dir__)
load File.expand_path('lib/tag-link-popup/import-parser.rb', __dir__)
load File.expand_path('lib/tag-link-popup-mode-setting.rb', __dir__)
load File.expand_path('lib/tag-link-popup-validator.rb', __dir__)

DiscoursePluginRegistry.serialized_current_user_fields << 'tag_link_popup_mode'

after_initialize do
  User.register_custom_field_type 'tag_link_popup_mode', :string
end
