TagLinkPopup::Engine.routes.draw do
  get "/" => "tag_link_popup#index"
  post "/import" => "tag_link_popup#import"
end
