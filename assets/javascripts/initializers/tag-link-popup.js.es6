import {
  withPluginApi
} from "discourse/lib/plugin-api"
import User from "discourse/models/user"
import {
  escapeExpression
} from "discourse/lib/utilities"
import getURL from "discourse-common/lib/get-url"
import {
  helperContext
} from "discourse-common/lib/helpers"
import {
  iconHTML
} from "discourse-common/lib/icon-library"

// Translations

function translate(key, data = {}) {
  return I18n.t("js.tag_link_popup." + key, data)
}

// SiteSettings

let currentSettings, currentUser, settingPrefix = "tag_link_popup_"

function getExternalTagUrl(tagSlug) {
  return getSetting("external_tag_url_pattern").replace("{tag-slug}", tagSlug)
}

function parseTags(string) {
  return string.split("|").reduce((acc, item) => {
    if (item.includes(":")) {
      let [tag, slug] = item.split(":")
      acc[tag] = slug
    } else {
      acc[item] = item
    }
    return acc;
  }, {});
}

function getSetting(key) { return currentSettings[settingPrefix + key] }

// Rendering

function tagLinkRender(tag, params) {
  params = params || {}
  const classList = ["discourse-tag"]
  const siteSettings = helperContext().siteSettings
  const style = params.style || siteSettings.tag_style
  const tagName = params.tagName || "a"
  const visibleName = escapeExpression(tag)
  let path
  tag = visibleName.toLowerCase()
  if ((params.isPrivateMessage || params.pmOnly) && User.current()) {
    const username = params.tagsForUser ? params.tagsForUser : User.current().username
    path = `/u/${username}/messages/tags/${tag}`
  } else {
    path = "/tag/" + tag
  }
  if (style) {
    classList.push(style)
  }
  if (params.size) {
    classList.push(params.size)
  }
  let classes = classList.join(" ")
  let data = `data-href="${getURL(path)}" data-tag-name="${tag}"`
  let html = `<span class="${classes}" ${data}>${visibleName}</span>`
  if (params.count) {
    html += " <span class='discourse-tag-count'>x" + params.count + "</span>"
  }
  return html
}

// Popup

function createLink(parent, data, iconClass, translation) {
  let link = document.createElement("a")
  link.classList.add("btn")
  link.href = data.href
  let translated_text = translate(translation, data)
  link.innerHTML = translated_text + " " + iconHTML(iconClass)
  parent.appendChild(link)
  return link
}

function createPopup(parent, externalUrl, tag, tagSlug) {
  let div = document.createElement("div")
  let internalUrl = parent.dataset.href || parent.href
  div.classList.add("tag-link-popup")
  let internalLink = createLink(div, {href: internalUrl, tag: tag}, "fab-discourse", "internal_link_text")
  let externalLink = createLink(div, {href: externalUrl, slug: tagSlug}, "external-link-alt", "external_link_text")
  parent.appendChild(div)
  let callback = (event) => {
    let nodes = [div, externalLink, internalLink]
    if (!nodes.includes(event.target)) {
      parent.removeChild(div)
      document.removeEventListener("mousedown", callback)
    }
  }
  document.addEventListener("mousedown", callback)
}

// Find and update

function getTagFromHash(element) {
  if (element.host == window.location.host) {
    let regex = RegExp("^\/(tag\/(?<tag>[^\/]+)|u\/[^\/]+\/messages\/tags(?<user_pm_tag>[^\/]+))$")
    let groups = regex.exec(element.pathname).groups
    return groups.tag || groups.user_pm_tag
  }
}

function onExternalLinkClick(event, element, tagSlug) {
  if ([event.target, event.target.parentNode].includes(element)) {
    window.location.href = getExternalTagUrl(tagSlug)
    return false
  }
}

function onInternalLinkClick(event, element) {
  if ([event.target, event.target.parentNode].includes(element)) {
    window.location.href = element.dataset.href || element.href
    return false
  }
}

function onPopupLinkClick(event, element, tag, tagSlug) {
  if ([event.target, event.target.parentNode].includes(element)) {
    let externalUrl = getExternalTagUrl(tagSlug)
    createPopup(element, externalUrl, tag, tagSlug)
    return false
  }
}

function updateLink(element, tags, tag) {
  if (!tags[tag]) {
    element.onclick = (event) => {
      return onInternalLinkClick(event, element)
    }
  } else if (tag) {
    let mode = currentUser.custom_fields.tag_link_popup_mode || getSetting("mode")
    switch (mode) {
      case "external_link":
        element.onclick = (event) => {
          return onExternalLinkClick(event, element, tags[tag])
        }
        break
      case "internal_link":
        element.onclick = (event) => {
          return onInternalLinkClick(event, element)
        }
        break
      case "popup":
        element.onclick = (event) => {
          return onPopupLinkClick(event, element, tag, tags[tag])
        }
        break
    }
  }
}

function findAndUpdate(data) {
  if (data.name == settingPrefix + "tags_list") {
    let tags = parseTags(data.value)
    document.querySelectorAll(".discourse-tag[data-tag-name]").forEach((tagLink) => {
      let tag = tagLink.dataset.tagName
      updateLink(tagLink, tags, tag)
    })
    document.querySelectorAll(".hashtag").forEach((element) => {
      let tag = getTagFromHash(element)
      updateLink(element, tags, tag)
    })
  } else {
    currentSettings[data.name] = data.value
    if (data.name.startsWith(settingPrefix)) {
      findAndUpdate({name: settingPrefix + "tags_list", value: getSetting("tags_list")})
    }
  }
}

// Initializer

export default {
  name: "tag-link-popup",
  initialize(data) {
    withPluginApi("0.8.31", (api) => {
      let application = api.container.owner.application
      currentSettings = application.SiteSettings
      currentUser = application.currentUser
      api.addTagsHtmlCallback(() => {
        let elements = document.querySelectorAll(".discourse-tag[data-tag-name]")
        if (elements.length > 0) {
          findAndUpdate({name: settingPrefix + "tags_list", value: getSetting("tags_list")})
        }
      })
      api.replaceTagRenderer(tagLinkRender)
      const messageBus = api.container.lookup("message-bus:main")
      messageBus.subscribe("/client_settings", findAndUpdate)
      api.modifyClass("controller:preferences/interface", {
        actions: {
          save () {
            this.get("saveAttrNames").push("custom_fields")
            this._super()
          }
        }
      })
    })
  }
}