import DiscourseRoute from 'discourse/routes/discourse'

export default DiscourseRoute.extend({
  controllerName: "tag-link-popup",

  model(params) {
    return ["csv", "json", "text"]
  },

  renderTemplate() {
    this.render("tag-link-popup")
  }
})