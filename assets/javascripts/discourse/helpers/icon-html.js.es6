import {
  iconHTML
} from "discourse-common/lib/icon-library"

export default Ember.Helper.helper(function (params) {
  let html = iconHTML(params[0])
  return Ember.String.htmlSafe(html)
})