import discourseComputed from "discourse-common/utils/decorators";

export default Ember.Component.extend({
  actions: {
    save() {
      console.log(this.get("user").custom_fields)
    },
    setMode(mode) {
      this.set("mode", mode)
      this.set("user.custom_fields.tag_link_popup_mode", mode)
    }
  },

  init() {
    this._super(...arguments)
    let user_setting = this.get("user.custom_fields.tag_link_popup_mode")
    let global_setting = this.get("siteSettings.tag_link_popup_mode")
    this.set("mode", user_setting || global_setting)
  },

  @discourseComputed
  modes() {
    return ["external_link", "internal_link", "popup"].map((value) => {
      return { name: I18n.t(`tag_link_popup.modes.${value}`), value: value };
    });
  }
})