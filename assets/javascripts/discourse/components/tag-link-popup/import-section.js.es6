import { action } from '@ember/object'
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  importService: Ember.inject.service(),
  router: service(),

  @action
  cancelFile() {
    this.set("file", false)
    this.resetStatus()
  },

  fileFormat() {
    switch(this.type) {
      case "csv":
        return `<pre><code class="lang-nohighlight">javascript,javascript
js,javascript</code></pre>`
      case "json":
        return `<pre><code class="lang-json hljs">{<span class="hljs-attr">"javascript"</span>: <span class="hljs-string">"javascript"</span>, <span class="hljs-attr">"js"</span>: <span class="hljs-string">"javascript"</span>}
</code></pre><p>or:</p><pre><code class="lang-json hljs">[<span class="hljs-string">"javascript"</span>]
</code></pre>`
      case "text":
        return `<pre><code class="lang-nohighlight">javascript
js:javascript</code></pre>`
    }
  },

  init() {
    this._super(...arguments)
    this.set("fileFormat", Ember.String.htmlSafe(this.fileFormat()))
    this.get("importService").on("fileSelected", this, "onFileSelected")
    this.resetStatus()
  },

  humanFileSize(bytes, si = false, dp = 2) {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
      return bytes + ' B';
    }

    const units = si
      ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10 ** dp;

    do {
      bytes /= thresh;
      ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);

    return bytes.toFixed(dp) + ' ' + units[u];
  },

  onFileSelected(data) {
    if(data.type === this.type) {
      let date = new Date(data.file.lastModified)
      let formattedDate = moment(date).format("LLLL")
      let fileName = data.file.name.split(".").slice(0, -1).join(".")
      let fileSize = this.humanFileSize(data.file.size)
      this.set("file", data.file)
      this.set("status", `${fileName} (${fileSize} - ${formattedDate})`)
    }
  },

  @action
  selectFile(event) {
    this.get("importService").trigger("fileSelect", this.type)
  },

  @action
  sendFile() {
    var data = new FormData()
    data.append("file", this.file)
    let file = this.file
    this.set("status", I18n.t("js.tag_link_popup.sending_file", this.file))
    this.set("file", false)
    let request = new XMLHttpRequest()
    let token = document.querySelector('meta[name="csrf-token"]').content
    request.open("POST", window.location.href + "/import.json", false)
    request.setRequestHeader("X-CSRF-Token", token);
    request.send(data)
    let response = JSON.parse(request.response)
    if(response.status == "ok") {
      let data = {queryParams: {filter: "plugin:tag-link-popup"}}
      this.get("router").transitionTo("adminSiteSettings", data)
    } else if(request.status == 200) {
      alert(response.message)
      this.resetStatus()
    } else {
      alert("This should not happen! Please report a bug!")
    }
  },

  resetStatus() {
    this.set("status", I18n.t("js.tag_link_popup.select_import_file", {type: this.type}))
  }
})