export default Ember.Component.extend({
  classNames: ["hidden-upload-field"],
  importService: Ember.inject.service(),
  tagName: "input",
  attributeBindings: ["accept", "change", "inputType:type"],
  inputType: "file",

  change(event) {
    let data = {file: event.target.files[0], type: this.type}
    this.get("importService").trigger("fileSelected", data)
  },

  init() {
    this._super(...arguments)
    switch(this.type) {
      case "csv":
        this.set("accept", "text/csv")
        break;
      case "json":
        this.set("accept", "application/json")
        break;
      case "text":
        this.set("accept", "text/plain")
        break
    }
    this.get("importService").on("fileSelect", this, "onFileSelect");
  },

  onFileSelect(type) {
    if(type === this.type) {
      document.getElementById(this.get("elementId")).click()
    }
  },

  willDestroyElement() {
    this.get("importService").off("fileSelect", this, "onFileSelect");
  }
})