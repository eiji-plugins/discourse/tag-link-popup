# TagLinkPopup

Change a standard tag link to popup with standard and external links.

## Installation

Follow [Install a Plugin](https://meta.discourse.org/t/install-a-plugin/19157)
how-to from the official Discourse Meta, using `git clone https://github.com/Eiji7/tag-link-popup.git`
as the plugin command.

## Configuration

This plugin have 4 options:

1. `tag_link_popup_enabled` - it's required option for enabling/disabling this plugin
2. `tag_link_popup_external_tag_url_pattern` - a configurable pattern for external tag url
for example: https://example.com/tags/{tag-slug}
3. `tag_link_popup_tags_list` - a list of tags for which you want to create a popup for
4. `tag_link_popup_update_interval` - a time in `ms` (milliseconds) for a simple loop which lookups and updates tag links

## Usage

Make sure this plugin is enabled and properly configured. Once everything is ready simply click on desired tag link.

## Feedback

If you have issues or suggestions for the plugin, please bring them up on
[Discourse Meta](https://meta.discourse.org).
